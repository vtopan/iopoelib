"""
PoE network querying - trade API client.

Author: Vlad Topan (vtopan/gmail).
"""

import ast
import json
import re
import struct
import time
from urllib.parse import urlencode

import requests

from iolib import net as ionet


SOLD_DELTA = 10000

API_URL = 'http://api.pathofexile.com/'
CID_URL = 'https://www.pathofexile.com/api/trade/data/change-ids'

ERRMAP = {
    1:"Resource not found",
    2:"Invalid query",
    3:"Rate limit exceeded",
    4:"Internal error",
    5:"Unexpected content type",
    6:"Forbidden",
    7:"Temporarily Unavailable",
    8:"Unauthorized",
    }

FRAME_TYPES = {
    # frameType values
    0:'normal',
    1:'magic',
    2:'rare',
    3:'unique',
    4:'gem',
    5:'currency',
    6:'div-card',
    7:'quest-item',
    8:'prophecy',
    9:'relic',
    }

TIME_FORMAT = '%Y-%m-%dT%H:%M:%SZ'

RX = {
    'price':(r'^~(b/o|price) +([\d\./]+) +([\w-]+)$',),
    'fraction':(r'^(\d+)/([1-9]\d*)$',),
    'next-id':('"next_change_id":"([^"]+)"',),
    'pn-cid':(r'>((?:\d{3,}-){4}\d{3,})<',),
    }
for k in RX:
    RX[k] = re.compile(*RX[k])



class TradeAPIClient:
    """
    Trade API client.

    :param league: League name.
    :param sold_delta: Consider an item finally sold after this many changes.
    :param caching: Cache web request results if True.
    :param err: Error logging function.
    """


    def __init__(self, league=None, sold_delta=SOLD_DELTA, caching=True, err=print):
        self.league = league
        self.sold_delta = sold_delta
        self.downloaded = 0
        self.caching = caching
        self.err = err


    def download(self, url, cache=60, headers=None):
        """
        Download from the given API URL and parse JSON response.
        """
        info, res = {}, None
        if not self.caching:
            cache = 0
        elif cache is None:
            cache = 60
        data = ionet.download(url, cache=cache, info=info, headers=headers)
        self.downloaded += len(data)
        try:
            jdata = json.loads(data.decode())
        except Exception as e:
            self.err(f'[!] Failed parsing returned JSON: {e}!\n{data}')
            jdata = {}
        if info.get('code', 200) != 200:    # not set if taken from cache
            err = jdata.get('error', {})
            ecode, emsg = err.get('code'), err.get('message')
            self.err(f'Request for {url} failed: {info["code"]} ({ecode} / {emsg})!')
        else:
            res = json.loads(data.decode('utf8', errors='replace'))
        return res


    def api_get(self, endpoint, cache=None, **kwargs):
        """
        Get data from an API endpoint.
        """
        args = {k:v for k,v in kwargs.items() if v is not None}
        if args:
            endpoint = f'{endpoint}?{urlencode(kwargs)}'
        url = API_URL + endpoint
        return self.download(url, cache=cache, headers={'Accept-Encoding':'gzip'})


    def get_leagues(self, league_type='main'):
        """
        Get league info.

        :param league_type: League type ("main", "event", "season", "all").
        :return: dict mapping league name (ID) to an info dict {'start':..., 'end':..., 'url':...}
        """
        data = self.api_get('leagues', type=league_type)
        return {e['id']:{'url':e['url'], 'start':parse_time(e['startAt']),
                'end':parse_time(e['endAt'])} for e in data}


    def current_cid(self):
        """
        Retrieves the current CID (change ID) from the official API.
        """
        res = self.download(CID_URL)
        if res:
            return res['psapi']
        return res


    def get_stashes_raw(self, cid=None, delta=None, cache=None):
        """
        Get the stashes for the given change ID.

        See https://pathofexile.gamepedia.com/Public_stash_tab_API for reference.
        """
        if cache is None:
            cache = 999999 if self.caching else 0
        if cid is None:
            cid = '0-0-0-0-0' if not cid_delta else self.current_cid()
        if delta:
            cid = cid_delta(cid, delta)
        res = self.api_get('public-stash-tabs', id=cid, cache=cache)
        res['cid'] = cid
        return res



def cid_delta(cid, delta):
    """
    Apply a numeric delta to a change ID (cid).
    """
    cid = '-'.join(str(int(x) + delta) for x in cid.split('-'))
    return cid


def parse_time(s):
    """
    Parse a time string, return a timestamp.
    """
    if not s:
        return 0
    return time.mktime(time.strptime(s, TIME_FORMAT))


def parse_price(pstr, currency_map=None):
    """
    Parse a price string.

    :param currency_map: If given, map currency names through this (if present).
    :return: {} or {'price': ..., 'currency': '...', 'type': 'b/o'|'price'} with an optional
        'min_amount' field.
    """
    result = {}
    if not pstr:
        return result
    m = RX['price'].search(pstr)
    if m:
        m = m.groups()
        price = m[1]
        result['type'] = m[0]
        result['currency'] = currency_map.get(m[2], m[2]) if currency_map else m[2]
        if price.lstrip('0'):
            try:
                price = ast.literal_eval(price.lstrip('0'))
            except ValueError:
                frac = RX['fraction'].search(price)
                if frac:
                    frac = frac.groups()
                    result['min_amount'] = int(frac[1])
                    price = int(frac[0]) / result['min_amount']
                else:
                    price = None
            except Exception:
                price = None
        else:
            price = 0
        result['price'] = price
    return result


if __name__ == '__main__':
    tc = TradeAPIClient('synthesis')
    cid = tc.current_cid()
    print(f'Current CID: {cid}')
    stashes = tc.get_stashes_raw(cid=cid, delta=-1000)
    json.dump(stashes, open('stashes.json', 'w'), indent=2, sort_keys=1)

