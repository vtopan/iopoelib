"""
PoE network querying - misc functions.

Author: Vlad Topan (vtopan/gmail).
"""


def wiki_url(name):
    """
    Return the wiki URL for the given name.
    """
    return f'https://pathofexile.gamepedia.com/{name.replace(" ", "_")}'

