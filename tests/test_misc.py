import re


from iopoelib import misc


def test_ctrlc_parser():
    lst = open('poe-ctrlc-samples.txt').read().strip().split('\n\n\n')
    parsed = [misc.parse_ctrlc(e) for e in lst]
    for i in range(len(lst)):
        assert(parsed[i])
    assert('flask' in parsed[0]['types'])
    assert('flask' in parsed[1]['types'])
    assert('Instant Recovery' in parsed[1]['mods'])
    assert('Vaal Orb' in parsed[2]['types'])
    assert('sextant' in parsed[3]['types'])
    assert(parsed[4]['quality'] == 13)
    assert('scarab' in parsed[5]['types'])
    assert('fossil' in parsed[6]['types'])
    assert(parsed[7]['rarity'] == 'unique')
    assert(parsed[7]['price'][0] == 80)
    assert('Passives in Radius can be Allocated without being connected to your tree' in parsed[8]['mods'])
    assert('map' in parsed[9]['types'])
    assert(parsed[9]['itemlevel'] == 75)
    assert('Underground Sea Map' in parsed[10]['types'])
    assert('fragment' in parsed[11]['types'])
    assert('fragment' in parsed[12]['types'])
    assert('Divine Vessel' in parsed[13]['types'])
    assert('Imperial Staff Piece' in parsed[14]['types'])
    assert(parsed[15]['tier'] == 'shrieking')
    assert('net' in parsed[16]['types'])
    assert(parsed[17]['requirements']['Level'] == 8)
    assert(parsed[18]['links'] == 6)
    assert('resonator' in parsed[19]['types'])
    assert('fragment' in parsed[20]['types'])
    assert('map' in parsed[21]['types'])
    assert('divinationcard' in parsed[22]['types'])
    

def test_name_parsing():
    h = open('tmp', 'w')
    for line in open('poe-item-name-samples.txt'):
        line = line.strip()
        if not line:
            continue
        name, result = line.split('|')
        result = result.strip().split(',')
        if misc.get_item_types(name) != result:
            raise AssertionError(f'Failed parsing {name}: {misc.get_item_types(name)}')
        
    
if __name__ == '__main__':
    test_ctrlc_parser()
    test_name_parsing()
    