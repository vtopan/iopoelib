"""
PoE network querying - poe.ninja.

Author: Vlad Topan (vtopan/gmail).
"""

import requests


PN_CATEGORIES = ('Essence', 'Map', 'UniqueArmour', 'UniqueFlask', 'UniqueWeapon', 'UniqueAccessory',
        'UniqueJewel', 'Fragment', 'Currency', 'Prophecy', 'UniqueMap', 'DivinationCard',
        'Fossil', 'Resonator', 'Scarab', 'SkillGem', 'HelmetEnchant', 'Oil', 'Watchstone',
        'Incubator', 'BaseType', 'Beast', 'Vial')


def poeninja_query_prices(category, league, date=None):
    """
    Query poe.ninja prices for the given category.

    If successful, the 'lines' field in the result is a list of dicts.

    For the 'Currency' and 'Fragment' categories, which are treated as currencies, the interesting
    fields are 'currencyTypeName', 'chaosEquivalent' and 'receive' / 'pay', which both (if non-null)
    are dicts with the relevant subkeys 'value', 'pay_currency_id', 'get_currency_id' (see
    'currencyDetails'). The response dict also has a 'currencyDetails' field which is a list of dicts
    with 'icon', 'name' and 'poeTradeId' subfields.

    For the other categories, the relevant fields are 'name', 'icon', 'chaosValue', 'exaltedValue'.
    Other fields which may be present / relevant: 'mapTier', 'basType', 'stackSize', 'variant',
    'links', 'itemClass', 'implicitModifiers' (list), 'explicitModifiers' (list), 'corrupted',
    'gemLevel', 'gemQuality', 'itemType'. The "historic" data is present under the 'sparkline' key
    as a dict {'data': [N1, N2, ...], 'totalChange': N}.

    :param category: One of `PN_CATEGORIES`.
    :param league: League name.
    :param date: Optional, must have '%Y-%m-%d' format if present.
    """
    if category not in PN_CATEGORIES:
        raise ValueError(f'Unknown poe.ninja category {category}!')
    catgroup = 'currency' if category in ('Currency', 'Fragment') else 'item'
    url = f'https://poe.ninja/api/data/{catgroup}overview?type={category}&league={league}'
    if date:
        url += f'&date={date}'
    res = requests.get(url)
    if res.status_code != 200:
        raise ValueError(f'[!] poe.ninja request for {category}@{league} failed: HTTP {res.status_code}!')
    result = res.json()
    return result


def poeninja_brief_result(result):
    """
    Extracts the relevant information from a poe.ninja result.

    :return: A dict mapping names to prices in chaos.
    """
    res = {}
    as_currency = 'currencyTypeName' in result['lines'][0]    # "currency" or "item" result
    for e in result['lines']:
        if as_currency:
            res[e['currencyTypeName']] = e['chaosEquivalent']
        else:
            res[e['name']] = e['chaosValue']
    return res


def poeninja_category(itemtypes, rarity=None):
    """
    Tries to guess the poe.ninja category based on the item's types and rarity.
    """
    if 'gem' in itemtypes:
        return 'SkillGem'
    if 'divinationcard' in itemtypes:
        return 'DivinationCard'
    if rarity == 'unique':
        for e in ('armour', 'flask', 'weapon', 'accessory', 'jewel', 'map'):
            if e in itemtypes:
                return f'Unique{e.capitalize()}'
    for e in ('scarab', 'resonator', 'fragment', 'fossil', 'resonator', 'essence', 'prophecy',
            'map', 'currency'):
        if e in itemtypes:
            return e.capitalize()
    return None


