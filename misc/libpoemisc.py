"""
PoE misc tools.

Author: Vlad Topan (vtopan/gmail).
"""

import ast
import collections
import re


BASE_ALIASES = {
    # NOT for currency items (including div cards)
    'BodyArmour': 'Armour|Brigandine|Chainmail|Chestplate|Coat|Doublet|Dragonscale|Jacket'
            '|Jerkin|Garb|Hauberk|Lamellar|Leather|Mantle|Plate|Raiment|Regalia|Ringmail|Robe|Silks'
            '|Tunic|Vest|Vestment|Wrap|Wyrmscale',
    'Boots': 'Caligae|Greaves|Shoes|Slippers',
    'Gloves': 'Bracers|Gauntlets|Mitts',
    'Helmet': 'Bascinet|Burgonet|Cage|Cap|Circlet|Coif|Crown|Hat|Helm|Hood|Mask|Pelt|Sallet|Tricorne|Wreath',
    'Shield': 'Buckler|Bundle',

    'Quiver': None,

    'Belt': 'Obi|Sash|Vise',
    'Amulet': 'Talisman',
    'Ring': 'Hoop',

    'Jewel': None,

    'Flask': None,

    'Axe': 'Chopper|Cleaver|Fleshripper|Hatchet|Labrys|Poleaxe|Splitter|Tomahawk|Woodsplitter',
    'Bow': None,
    'Claw': 'Awl|Blinder|Fist|Gouger|Paw|Ripper|Stabber',
    'Dagger': 'Ambusher|Knife|Kris|Poignard|Sai|Shank|Skean|Stiletto|Trisula',  # also Boot Blade...
    'Mace': 'Breaker|Club|Flame|Gavel|Hammer|Mallet|Maul|Meatgrinder|Pernarch|Piledriver|Sledgehammer'
            '|Star|Steelhead|Tenderizer',
    'Rod': None,
    'Sceptre': 'Fetish|Sekhem',
    'Staff': 'Branch|Lathi|Quarterstaff',
    'Sword': 'Blade|Baselard|Cutlass|Estoc|Foil|Gladius|Grappler|Greatsword|Hook|Keyblade'
            '|Longsword|Pecoraro|Rapier|Sabre|Smallsword|Spike',
    'Wand': 'Horn',

    'Resonator': None,

    # Silver/Golden/Treasure Key; also fragment: Eber's/etc., * Reliquary Key
    'QuestItem': 'Keg|Passage|Key|Goddess',

    'Leaguestone': None,


    'Scarab': None,

    'Map': None,   # Map before Fragment for the Shaper guardian fragments
    'Fragment': 'Breachstone|Vessel|Chimera|Hydra|Minotaur|Phoenix|Grief|Hope|Ignorance|Rage|Dawn|Dusk|Midnight|Noon',

    'Piece': None,
    }

REV_ALIAS_MAP = {}
ALIAS_N_WORDS = {
    1: {'Ambusher', 'Awl', 'Baselard', 'Blinder', 'Chestplate', 'Cleaver', 'Cutlass', 'Estoc',
        'Fleshripper', 'Gavel', 'Gladius', 'Gouger', 'Grappler', 'Keyblade', 'Labrys', 'Longsword',
        'Mallet', 'Meatgrinder', 'Pecoraro', 'Pernarch', 'Piledriver', 'Poignard', 'Poleaxe',
        'Sabre', 'Sai', 'Sallet', 'Skean', 'Sledgehammer', 'Smallsword', 'Steelhead', 'Stiletto',
        'Tenderizer', 'Tomahawk', 'Tricorne', 'Trisula', 'Woodsplitter'},
    3: {'Resonator', 'Passage', 'Scarab', 'Piece', 'Feather', 'Dawn', 'Dusk', 'Midnight', 'Noon'},
    4: {'Goddess',},
    -4: {'Fragment',},    # 'Fragment' matches only Shaper fragments
    }
REV_ALIAS_N_WORDS = {}
ALIAS_23_WORDS = {
    'Amulet': {'Pearl',},
    'Jewel': {'Eye',},
    'Claw': {'White',},
    'Gloves': {'Silk',},
    'Flask': {'Life', 'Mana', 'Hybrid',},
    'Breachstone': {'Charged', 'Enriched', 'Pure',},
    'Map': {'Blocks', 'Cathedral', 'Caverns', 'Chamber', 'Chambers', 'City', 'Crypt', 'Forest',
            'Geyser', 'Hollow', 'Island', 'Lake', 'Mansion', 'Mine', 'Nest', 'Pools', 'Pyramid',
            'River', 'Ruin', 'Ruins', 'Sea', 'Sewer', 'Spring', 'Square', 'Temple', 'Tomb',
            'Valley', 'Vents', 'Wood'},   # Shrine: Overgrown, Temple: Crimson, Ivory, Moon, Vaal
    'Key': ('Reliquary',),
    'Quiver': ('Arrow',),
    'Shield': ('Spiked', 'Spirit', 'Kite', 'Round', 'Tower',),
    }
# exceptions: Sekhem / Tyrant's Sekhem
BASE_ALIAS_RX = None


RX = {
    'wtb':(r'(?:@(To|From) (?:\<([^ ]+)> )?([^: ]+): )?wtb (\d+)?(.+?) (?:@ ([\d\.]+) '
        r'([\w \'-]+?) )?in (\w+)(?: stash "([^"]*)" @(\d+),(\d+))?\.?(.*)',),
    'num': (r'\d[\.\d]*',),
    }
for k in RX:
    RX[k] = re.compile(*RX[k])

WTB_Message = collections.namedtuple('WTB_Message', ('direction', 'guild', 'sender', 'count',
        'item', 'price', 'currency', 'league', 'stash', 'x', 'y', 'comment', 'errors'))


def parse_ctrlc(text):
    """
    Parse in-game Ctrl+C texts.
    """
    info = {}
    text = text.replace('\r\n', '\n').strip()
    if not (text.startswith('Rarity: ') and '--------\n' in text):
        return info
    sections = text.strip().split('\n--------\n')
    header = sections[0].strip().split('\n')
    info['types'] = ['item']
    info['tags'] = []
    # rarity vs type
    rarity = header[0].split(': ')[1]
    if len(header) > 2:
        info['name'] = header[1]
    subtype = header[-1]
    if subtype.startswith('Superior '):
        subtype = subtype[9:]
    info['types'].append(subtype)
    if rarity not in ('Magic', 'Rare', 'Unique'):
        if rarity == 'Normal' and sections[-1] == 'Relic Unique':
            rarity = 'Relic'
            del sections[-1]
        elif rarity in ('Currency', 'Gem', 'Divination Card'):
            info['types'][0] = rarity.lower().replace(' ', '')
            rarity = None
    if rarity:
        info['rarity'] = rarity.lower()
    if sections[1].startswith('Map Tier:'):
        info['types'][0] = 'map'
    elif rarity == 'Normal':
        m = re.search(' (Scarab)$|^(Fragment) of the |\'s (Key)$| (Breachstone)$|^(Sacrifice) at ', subtype)
        if m:
            info['types'][0] = [x for x in m.groups() if x][0].lower()
            if info['types'][0] in ('breachstone', 'key', 'sacrifice'):
                info['types'][0] = 'fragment'
            del info['rarity']
        elif subtype == 'Divine Vessel':
            info['types'][0] = 'fragment'
            del info['rarity']
    elif info['types'][0] == 'currency':
        m = re.search(' (Essence) of | (Net|Fossil)$', subtype)
        if m:
            ttype = [x for x in m.groups() if x][0].lower()
            info['types'].append(ttype)
            if ttype == 'essence':
                info['tier'] = subtype.split(' ')[0].lower()
    if info['types'][0] == 'item':
        types = get_item_types(subtype)
        if types:
            info['types'][1:] = types
    if info['types'][0] == 'gem':
        lines = sections[1].split('\n')
        info['gem_tags'] = [x.lower() for x in lines[0].split(', ')]
    for e in ('Quality', 'Map Tier', 'Level'):
        m = re.search(rf'^{e}: \+?(\d+)', sections[1], flags=re.M)
        if m:
            info[e.lower().split(' ')[-1]] = int(m.groups()[0])
    if info['types'][0] == 'currency':
        m = re.search(' (Resonator|Sextant)$', subtype)
        if m:
            info['types'].append(m.groups()[0].lower())
    unk_sections = []
    for sec in sections[1:]:
        if sec in ('Shaper Item', 'Elder Item', 'Corrupted', 'Unidentified', 'Abyss'):
            info['tags'].append(sec.replace(' ', '').lower())
        elif sec.startswith('Item Level'):
            info['itemlevel'] = int(sec.split(': ')[1])
        elif sec.startswith('Requirements:'):
            lines = sec.split('\n')[1:]
            lst = [re.sub(r' \((augmented|unmet)\)', '', x).split(': ', 1) for x in lines]
            info['requirements'] = dict((x[0], int(x[1])) for x in lst)
        elif sec.startswith('Sockets: '):
            info['sockets'] = sec[9:].strip()
            links = max(len(x) for x in info['sockets'].replace('-', '').split(' '))
            if links >= 5:
                info['links'] = links
        elif sec.startswith('Vaal ') and 'gem' in info['types']:
            info['types'][1] = sec
            info['tags'].append('vaal')
            info['types'].append('vaal gem')
        elif re.search('^(Note|Radius): (.+)', sec):
            m = re.search('^(Note|Radius): (.+)', sec).groups()
            info[m[0].lower()] = m[1]
            if m[0] == 'Note':
                p = parse_price_note(m[1])
                if p:
                    info['price'] = p[1:2]
        elif sec.startswith('Talisman Tier: '):
            info['tier'] = int(sec[-1])
        elif 'Right-click to add this prophecy' in sec:
            info['types'][0] = 'prophecy'
            del info['rarity']
        elif re.search('^(Place into an |Travel to this Map|This item will transform)', sec):
            continue
        elif sec.startswith('Stack Size:'):
            info['count'] = int(sec.split(': ', 1)[1].split('/', 1)[0].replace(',', ''))
        else:
            unk_sections.append(sec)
    if info['types'][0] == 'item':
        info['attributes'] = {}
        info['mods'] = []
        if unk_sections:
            if info['rarity'] in ('unique', 'relic'):
                del unk_sections[-1]
            if info['types'][-1] in ('flask', 'talisman'):
                del unk_sections[-1]
            for sec in unk_sections:
                for e in sec.split('\n'):
                    if ': ' in e:
                        e = e.split(': ', 1)
                        v = e[1].replace(' (augmented)', '')
                        oldv = info['attributes'].get(e[0])
                        if oldv:
                            v = (oldv if type(oldv) is list else [oldv]) + [v]
                        info['attributes'][e[0]] = v
                    else:
                        info['mods'].append(e)
    return info


def parse_price_note(s):
    """
    Parse a price note.

    :return: tuple('b/o'|'price', value or None (unparsable), currency, value_as_string).
    """
    m = re.search(r'~(b/o|price)\s+([\d\./]+)\s+(.+)', s)
    if m:
        m = m.groups()
        try:
            price = ast.literal_eval(m[1])
        except (SyntaxError, ValueError):
            price = 0
        return (m[0], price, m[2], m[1])
    return None


def shorten_wtb_pm(s):
    """
    Shorten "wtb" PMs.
    """
    s = re.sub('(?:Hi, )?I(?:\'d| would) like to buy your ', 'wtb ', s)
    s = re.sub(r'(?: listed for | for my )(?=\d)', ' @ ', s)
    s = re.sub(r'\(stash (?:tab )?"([^"]*?)"; (?:position: )?left (\d+), top (\d+)\)', r'stash "\1" @\2,\3', s)
    return s


def parse_wtb_pm(s, sender=None):
    """
    Parse a "wtb" PM.

    The .errors field is a list of parsing errors (if any).
    The .count and .price are int or float (0 if parsing failed, None if not present in message).

    :param sender: If the "@From ..." prefix is stripped, pass sender through this parameter (will
        be marked as a "From" message).
    :return: WTB_Message named tuple(direction, guild, sender, count, item, price, currency,
        league, stash, x, y, comment, errors) or None.
    """
    m = RX['wtb'].search(shorten_wtb_pm(s))
    if not m:
        return
    errors = []
    lst = list(m.groups()) + [errors]
    lst[4] = lst[4].strip()
    if sender and not lst[2]:
        lst[2] = sender
        lst[0] = 'From'
    for i in (3, 5, 9, 10):
        if lst[i]:
            try:
                lst[i] = float(lst[i])
                if i in (9, 10) or int(lst[i]) == lst[i]:
                    lst[i] = int(lst[i])
            except Exception as e:
                errors.append(f'Failed parsing {lst[i]} as float: {e}!')
                lst[i] = 0
    return WTB_Message(*lst)


def get_item_types(name_or_basetype):
    """
    Parse the item types from an item's name.

    :param name_or_basetype: Item's name (for identified magic items) or base type (for others).
    """
    # todo: add 1h vs 2h
    m = BASE_ALIAS_RX.search(name_or_basetype)
    if not m:
        return None
    subtype = m.groups()[0]
    if subtype == 'Blade':
        itype = 'dagger' if 'Boot Blade' in name_or_basetype else 'sword'
    else:
        itype = REV_ALIAS_MAP[subtype]
    words = name_or_basetype.strip().split(' ')
    pos = words.index(subtype)
    prev = words[pos - 1] if pos > 0 else None
    if subtype == 'Key':
        if prev == 'Reliquary':
            wcnt = 3
            itype = 'fragment'
        else:
            wcnt = 2
            if prev not in ('Silver', 'Golden', 'Treasure'):
                # Pale Council Key (Eber's, etc.)
                itype = 'fragment'
    elif subtype == 'Piece':
        wcnt = 4 if prev in ('Shield', 'Quiver') else 3
    elif subtype in ALIAS_23_WORDS:
        wcnt = 2
        if pos > 0 and prev in ALIAS_23_WORDS[subtype]:
            wcnt = 3
        if subtype == 'Map':
            if prev in ('Shrine', 'Temple') and pos > 1 \
                    and words[pos - 2] in ('Overgrown', 'Crimson', 'Ivory', 'Moon', 'Vaal'):
                wcnt += 1
            elif prev in ('Phoenix', 'Chimera', 'Hydra', 'Minotaur'):
                wcnt = 5
            if pos >= wcnt and words[pos - wcnt] == 'Shaped':
                wcnt += 1
    else:
        wcnt = REV_ALIAS_N_WORDS.get(subtype, 2)
    base_type = words[max(0, pos - wcnt + 1):pos + 1] if wcnt > 0 else words[pos:pos - wcnt]
    base_type = ' '.join(base_type)
    types = [base_type, itype]
    if itype in ('axe', 'bow', 'claw', 'dagger', 'mace', 'rod', 'sceptre', 'staff', 'sword', 'wand'):
        types.append('weapon')
    elif itype in ('bodyarmour', 'boots', 'gloves', 'helmet', 'shield'):
        types.append('armour')
    elif itype in ('belt', 'amulet', 'ring'):
        types.append('accessory')
    return types


def extract_mod_numbers(mod, replacement='#'):
    """
    Extract the numbers in a mod string.

    :return: (normalized_name, numbers_list).
    """
    if not mod:
        return (mod, [])
    m = RX['num'].findall(mod)
    if not m:
        return (mod, [])
    numbers = [float(x) if '.' in x else int(x) for x in m]
    mod = RX['num'].sub('#', mod)
    return mod, numbers


def init():
    """
    Initialize complex global constants.
    """
    global BASE_ALIAS_RX
    BASE_ALIAS_RX = re.compile('(?:^| )(%s)(?:$| of )' % '|'.join(f'{k}|{v}' if v else k
            for k, v in BASE_ALIASES.items()))
    for k, v in BASE_ALIASES.items():
        REV_ALIAS_MAP[k] = k.lower()
        if v:
            REV_ALIAS_MAP.update({x:k.lower() for x in v.split('|')})
    for k, v in ALIAS_N_WORDS.items():
        for e in v:
            REV_ALIAS_N_WORDS[e] = k


init()
