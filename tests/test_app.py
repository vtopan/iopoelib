import re

# from nose.tools import eq_

from iopoelib import app


def test_app():
    fh = app.log_handle()
    for e in app.read_log_line(fh, parse=True, filter=['pm', 'afk']):
        assert('pm' in e or 'afk' in e)
    for e in app.read_log_line(fh, filter=[], custom_rx=app.CUSTOM_LOG_RX['death'],
            timestamp=1):
        assert('timestamp' in e)
        assert('death' in e)


if __name__ == '__main__':
    test_app()

