"""
PoE network querying - pathofexile.com.

Author: Vlad Topan (vtopan/gmail).
"""

import requests


def poecom_fetch_results(ids, max_results=20):
    """
    Fetch the trade/exchange search results for the given ID list.

    :param max_results: Maximum number of results to retrieve.
    """
    results = []
    if ids:
        if max_results:
            ids = ids[:max_results]
        # can only get max. 10 results in a request
        for i in range(0, len(ids), 10):
            url = f'https://www.pathofexile.com/api/trade/fetch/{",".join(ids[i:i+10])}'
            # print(f'[#] Requesting {url}...')
            res = requests.get(url)
            if res.status_code != 200:
                raise ValueError(f'[!] Trade result retrieval failed: HTTP {res.status_code}! '
                        f'Message: {res.json().get("error", "unknown error")}')
            results += res.json()['result']
    return results


def poecom_query_trade(name=None, links=None, corrupted=None, rarity=None, league='Synthesis',
        priced=None, filters=None, type=None, quality=None, max_results=20, online=True,
        return_url=False):
    """
    Query pathofexile.com/trade.

    Price in results:
        {'listing': {
            'account': {'lastCharacterName': '...',
                        'name': '...',
                        'online': {'league': 'Synthesis', 'status': 'afk'}}, ...},
            'indexed': '2018-12-10T08:44:20Z',
            'price': {'amount': 1, 'currency': 'exa', 'type': '~price'},
            'stash': {'name': '...', 'x': ..., 'y': ...}, ...}

    :param name: Item name.
    :param links: Minimum linked sockets (5 or 6 usually).
    :param corrupted: True or False.
    :param rarity: 'normal', 'magic', 'rare' or 'unique'.
    :param league: League name.
    :param priced: Has a price set.
    :param filters: Additional, explicit filters.
    :param return_url: If true, return a tuple (search_url, results).
    :return: A list of {'id':..., 'item': {'frameType':..., 'explicitMods': [...], 'name': ...,
            etc.}, etc.}
    """
    # todo: filters
    j = {'query':{'filters':{}}, 'sort': {'price': 'asc'}}
    if online:
        j['query']['status'] = {'option': 'online'}
    if name:
        j['query']['name'] = name
    if type:
        j['query']['type'] = type
    if links:
        j['query']['filters']['socket_filters'] = {'filters': {'links': {'min': links}}}
    if corrupted is not None:
        j['query']['filters']['misc_filters'] = {'filters': {'corrupted': {'option':
                str(corrupted).lower()}}}
    if quality is not None:
        if 'misc_filters' not in j['query']['filters']:
            j['query']['filters']['misc_filters'] = {'filters': {}}
        j['query']['filters']['misc_filters']['filters'].update({'quality': {'min': quality}})
    if rarity:
        j['query']['filters']['type_filters'] = {'filters': {'rarity': {'option': rarity.lower()}}}
    if priced is not None:
        j['query']['filters']['trade_filters'] = {'filters': {'sale_type': {'option': 'priced'}}}
    # print('[#] Query parameters:')
    # import pprint; pprint.pprint(j)
    res = requests.post(f'https://www.pathofexile.com/api/trade/search/{league}', json=j)
    jres = res.json()
    if res.status_code != 200:
        raise ValueError(f'[!] Trade query failed: HTTP {res.status_code}! '
                f'Message: {jres.get("error", "unknown error")}')
    results = poecom_fetch_results(jres['result'], max_results=max_results)
    return results if not return_url else (poecom_get_query_url(jres['id'], league=league), results)


def poecom_query_exchange(currency, league='Synthesis', in_currency='chaos', max_results=20,
        return_url=False):
    """
    Query pathofexile.com/trade/exchange for how much a currency sells.

    :param currency: Currency name.
    :param league: League name.
    :param in_currency: In which currency to trade (default: 'chaos').
    :param return_url: If true, return a tuple (search_url, results).
    """
    j = {'exchange': {'have': ['chaos'], 'want': [currency], 'status': {'option': 'online'}}}
    # print('[#] Query parameters:')
    # pprint.pprint(j)
    res = requests.post(f'https://www.pathofexile.com/api/trade/exchange/{league}', json=j)
    jres = res.json()
    if res.status_code != 200:
        raise ValueError(f'[!] Trade query failed: HTTP {res.status_code}! '
                f'Message: {jres.get("error", "unknown error")}')
    results = poecom_fetch_results(jres['result'], max_results=max_results)
    return results if not return_url else (poecom_get_query_url(jres['id'], league=league,
            exchange=True), results)


def poecom_get_query_url(query_id, league='Synthesis', exchange=False):
    """
    Returns the web URL to be opened in browser for a given query_id.

    :param exchange: True if an exchange query, False if a normal one.
    """
    mode = 'exchange' if exchange else 'search'
    return f'https://www.pathofexile.com/trade/{mode}/{league}/{query_id}'


def poecom_get_trade_static_data():
    """
    Get the JSON of trade static data used by the official trade site.
    """
    url = 'http://www.pathofexile.com/api/trade/data/static'
    res = requests.get(url)
    if res.status_code != 200:
        raise ValueError(f'[!] Trade static data retrieval failed: HTTP {res.status_code}! '
                f'Message: {res.json().get("error", "unknown error")}')
    print(res.json()['result'])
    return res.json()['result']


def poecom_extract_currency_info(static_data=None):
    """
    Extract the mapping from currency name to short name/image from the result of
    `poecom_get_trade_static_data()`.

    :return: dict: currency_full_name: tuple(short_name, image_url).
    """
    if static_data is None:
        static_data = poecom_get_trade_static_data()
    cmap = {}
    for cat in ('currency', 'essences', 'fossils', 'resonators'):
        for e in static_data[cat]:
            cmap[e['text'].strip()] = (e['id'].strip(), ('https://www.pathofexile.com'
                    + e['image']) if 'image' in e else None)
    return cmap


def poecom_extract_trademap(static_data=None, include_type=False):
    """
    Extract the mapping from trade item (currency, scarab, fragment) name to short name
    and image from the result of `poecom_get_trade_static_data()`.

    :return: dict: currency_full_name: tuple(short_name, image_url).
    """
    if static_data is None:
        static_data = poecom_get_trade_static_data()
    cmap = {}
    for node in static_data:
        cat = node['id'].lower()
        for e in node['entries']:
            # the .strip() is necessary for e.g. "Fractured Fossil "
            t = (e['id'].strip(), ('https://www.pathofexile.com' + e['image']) if 'image' in e else None)
            if include_type:
                t = t + (cat,)
            cmap[e['text'].strip()] = t
    return cmap

