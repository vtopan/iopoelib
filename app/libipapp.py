"""
PoE application tools.

Author: Vlad Topan (vtopan/gmail).
"""

import datetime
import os
import re
import time

# if the follwing import fails, run pip install git+https://gitlab.com/vtopan/iolib
import iolib.win as win


VER = '0.1.0 (dec.2018)'

LOG_RX = {
    'area': r'^: You have entered (?P<area>[^\.\r\n]+)',
    'pm': r'^@From (?:<(?P<pm_guild>[^>]+)> )?(?P<pm_sender>[\w-]+): (?P<pm>.+)',
    'afk': r'^: AFK mode is now (?P<afk>O\w+)',
    'local': r'^(?P<local_sender>[a-zA-Z" -]+)(?:,[\'a-zA-Z" -]+)?: (?P<local>.+)',
    }
CUSTOM_LOG_RX = {e[0]:re.compile(*e[1]) for e in [
    ('death', (r'^: ([\w-]+) has been slain.',)),
    ]}
IGNORE_LOG_RX = {
    'local_sender': r'Web root|Enumerated (device for )?adapter|Queue file|Abnormal disconnect| passive skill |CreateSwapChain',
    }


def game_running():
    """
    Returns True if the game is running, False otherwise.
    """
    return bool(win.FindWindow(None, 'Path of Exile'))


def game_focused():
    """
    Returns True if the game window is focused (in foreground).
    """
    buf = win.unicode_buffer(512)
    h = win.GetForegroundWindow()
    win.GetWindowText(h, buf, 512)
    return buf.value == 'Path of Exile'
    
    
def get_poe_path():
    """
    Returns the path to the PoE folder (or None if the game isn't running).
    """
    hwnd = win.FindWindow(None, 'Path of Exile')
    if not hwnd:
        return None
    pid = win.DWORD(0)
    win.GetWindowThreadProcessId(hwnd, win.byref(pid))
    pid = pid.value
    # print(f'[#] PoE PID: {pid}')
    ph = win.OpenProcess(win.PROCESS_QUERY_LIMITED_INFORMATION, False, pid)
    if ph is None:
        raise OSError(f'[!] OpenProcess(pid={pid}) failed: {win.last_error()}')
    size = win.DWORD(2048)
    buf = win.unicode_buffer(size.value)
    res = win.QueryFullProcessImageName(ph, 0, buf, win.byref(size))
    if not res:
        raise OSError(f'[!] QueryFullProcessImageName() failed: {win.last_error()}')
    exe_path = str(buf.value)
    # print(f'[#] EXE path: {exe_path}')
    win.CloseHandle(ph)
    return os.path.dirname(exe_path)


def get_log_path():
    """
    Obtains the client.txt path if any instance of the game is running.
    """
    poe_path = get_poe_path()
    # print(f'[*] PoE path: {poe_path}')
    if not poe_path:
        return
    log_path = os.path.join(poe_path, 'logs\\client.txt')
    return log_path


def log_handle(log_file=None):
    """
    Get a log file handle from a file name, handle or None.
    """
    if not log_file:
        log_file = get_log_path()
    if hasattr(log_file, 'read'):
        fh = log_file
    else:
        fh = open(log_file, 'r', encoding='utf8', errors='replace')
    return fh


def read_log_tail(log_file=None, size=64 * 1024):
    """
    Read the last `size` bytes from client.txt.

    :param log_file: A file name, handle or None.
    :param size: Skip to EOF minus this many bytes before reading (default: 16K).
    """
    fh = log_handle(log_file)
    fsize = os.fstat(fh.fileno()).st_size
    if size < fsize:
        fh.seek(fsize - size)
    return fh.read()

    
def current_area(log_tail):
    """
    Extracts the current area from a log tail.
    """
    lst = re.findall('^[\d/]+ [\d:]+ \d+ \w+ \[[^\]]+\] ' + LOG_RX['area'][1:], log_tail, flags=re.M)
    return lst[-1] if lst else None
    

def read_log_line(log_file=None, callback=None, seek=None, parse=False, custom_rx=None, live=False,
        filter=None, timestamp=False):
    """
    Watch client.txt for area changes and pass lines to callback (or yield them if callback is None).

    The game must be runing.

    Examples:
        read_log_live(callback=print, live=True)
        read_log_live(parse=True, filter=['afk', 'pm'])
        read_log_line(log_file(), filter=[], custom_rx=CUSTOM_LOG_RX['death'], timestamp=1)

    Valid regex groups (what to expect if `parse` is True / possible filters):
        'area', 'afk', 'pm', 'local' and whatever the `custom_rx` defines.
    Note: 'pm' and 'local' will include a 'pm_sender' / 'local_sender' in the returned dict.

    :param callback: A function which gets the read line; return True to stop.
    :param log_file: A file name, handle or None.
    :param seek: If set, skip this many bytes (if negative, relative to EOF).
    :param parse: If True, parse the line with 'log_*' from RX and (only) yield/pass regex matched
        group dict.
    :param custom_rx: If given, must be a re.compile() result to run in addition to the default
        ones; if this regex defines no named groups, the returned key will be 'custom'
    :param live: I True, don't stop at EOF, wait for new lines to appear.
    :param filter: If set, must be a list/tuple of (default) regex group names to allow; if empty
    list or tuple, only the custom regex is applied (no default regexes).
    :param timestamp: If set, a 'timestamp' key is included in the returned dict.
    """
    # todo: make custom_rx a list & merge it with default to allow pluginable clients
    fh = log_handle(log_file)
    log_path = get_log_path()
    fsize = os.fstat(fh.fileno()).st_size
    if seek:
        if seek < 0:
            seek += fsize
        fh.seek(seek)
        fh.readline()  # jump over truncated current line
    if custom_rx:
        parse = True
    rxpat = '|'.join((LOG_RX[e] for e in filter) if filter is not None else LOG_RX.values())
    rx = re.compile(rxpat)
    while 1:
        if (not live) and fh.tell() == fsize:
            break
        while os.path.getsize(log_path) == fh.tell() and live:
            yield None
        line = fh.readline()
        if parse:
            try:
                pfx, line = line.split('] ', 1)
            except ValueError:
                continue
            m = None
            if custom_rx:
                m = custom_rx.search(line)
            if (not m) and (filter is None or filter):
                m = rx.search(line)
            if not m:
                continue
            res = {k:v for k, v in sorted(m.groupdict().items()) if v is not None}
            goto_next = 0
            for k in list(res):
                if k in IGNORE_LOG_RX and re.search(IGNORE_LOG_RX[k], res[k]):
                    del res[k]
                    for e in list(res):
                        if k in e or e in k:
                            del res[e]
                    if not res:
                        goto_next = 1
                        break
            if goto_next:
                continue
            if not res:
                res = {'custom': m.groups() or 'no-group'}
            if timestamp:
                res['timestamp'] = datetime.datetime.strptime(pfx[:19], '%Y/%m/%d %H:%M:%S')
        else:
            res = line
        if callback:
            if callback(res):
                break
        else:
            yield res


